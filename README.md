# The Promise of HTTP/2 Push

> HTTP/2 will make our applications faster, simpler, and more robust—a rare combination—by allowing us to undo many of the HTTP/1.1 workarounds previously done within our applications and address these concerns within the transport layer itself.

Ilya Grigorik **High Performance Browser Networking** *Chapter 12: HTTP/2* https://hpbn.co/http2/

## Intro
Thank you SFNode community and organisers for letting me speak tonight. Thanks to Instacart for hosting this event.

I am Sebastiaan, a web developer freelancing with startups in Singapore. I taught web development at General Assembly Singapore for 3 months. And I have been travelling around the world for the past 4 months.

I am visiting San Francisco for a week but it's too cold so I will have to return to South East Asia.

Catch me on Twitter @sebdeckers https://twitter.com/sebdeckers or GitLab https://gitlab.com/sebdeckers

## Legal Tech in Venice
Recently I got involved with Legalese.com which is half startup, half open source project, half academic research.

https://legalese.readme.io/

I set out to build a shiny new web app for Legalese.

I spent 2 weeks doped up on espresso, gelato, and charcuterie platters in Venice, Italy. (No compulsive reason, the team is simply nomadic and tries to be cultured.)

I ended up with no web app and instead built an HTTP/2 Server Push proof of concept.

https://gitlab.com/sebdeckers/push-demo

The last time I gave a talk on this topic was in 2014 and it was still called SPDY. Today it is HTTP/2 Server Push.

I achieved all goals:
- Learned new stuff (academic research!)
- Pushed lots of code (open source project!)
- Created nothing of commercial value (startup!)

## Problem: Round Trips
Web apps make many requests for documents, styles, scripts, and images. Each request means a round-trip, which means latency, which means poor performance, which means unhappy customers.

We reduce the number of requests using JavaScript bundlers like Webpack and Browserify, or CSS pre- and postprocessors, or image sprite sheets and stacking.

Let's see what that looks like.

- Instacart https://www.instacart.com/

## Solution: Enter HTTP/2 Server Push
HTTP/2 is supported by most modern browsers.

http://caniuse.com/#feat=http2

The adoption problem is server side.

http://isthewebhttp2yet.com/measurements/adoption.html

HTTP/2 offers multiplexed connections and header compression for faster data transfers. It uses the same semantics as HTTP/1.1 like request & response, headers & body. That makes it a quick win to place an HTTP/2 accelerator or CDN in front of your trusty old HTTP/1.1 webserver.

https://http2.github.io/faq/

But to reduce round trips we need to look at a feature of HTTP/2 called Server Push. These are unsolicited responses sent by the server to the client.

When a sufficiently advanced webserver receives a request for `/index.html` it may preempt future requests for `/app.js` and `/layout.css` by using a `PUSH_PROMISE` that delivers the content for those two requests along with the original HTML response. By the time the browser requires the JS and CSS files they will already be transferring or available in the cache.

## Demo

```
cd ~/Code/sebdeckers/push-demo
npm run clean
npm run build
http-server
open http://localhost:8080
http2server
open https://localhost:8080
```

#### Live: Try it yourself!

http://pushdemo.cf.sg

https://pushdemo.cf.sg

## Benefits

### Atomic Caching
Because a Push Promise only announces the file before it is sent, the client has the opportunity to cancel the push. This works both before the transfer starts or mid-way without any implications for other push promises or responses.

Where bundles and sprites need to update the entire blob, HTTP/2 clients only need to download the changed assets.

### Backwards Compatible
It can be implemented transparently to HTTP/1.1 clients and servers. They will continue to chug along with request—response round trips.

## Challenges

### Break the Browser
Browsers start auto-rejecting streams when receiving thousands of server pushes. Some web apps probably use this many source files and assets than this, considering the nature of dependencies. The complexity of code bases has only ever increased.

```
$ cp -r node_modules/ public/node_modules
$ http2server --max 10000
```

Firefox limits to ~5.3k server pushes.

Chrome limits to ~1000 server pushes.

### Certificates
While the HTTP/2 specification describes using the `Update` header to enable HTTP/2 over a plaintext HTTP connection, browsers do not support this at all. Instead they use the ALPN protocol inside TLS to negotiate the appliction layer protocol (i.e. SPDY, HTTP/2, or future protocols).

```
$ /usr/local/bin/curl -v --http2 -k -I https://www.cloudflare.com/
* Connected to www.cloudflare.com (198.41.214.162) port 443 (#0)
* ALPN, offering h2
* ALPN, offering http/1.1
...
* ALPN, server accepted to use h2
```

Fortunately self-signed certificates are easy to use and Letsencrypt is awesome.

### Not all HTTP/2 implementations are made equal
Safari does not currently support HTTP/2 Server Push. Not the latest version. Not the developer preview. Not even the WebKit Nightly.

Even Curl only recently gained HTTP/2 support. The OS X `curl` does not support it. Get it with `brew`, `ports`, or build from source.

https://simonecarletti.com/blog/2016/01/http2-curl-macosx/

Node.js lacks native support for HTTP/2 but actively working on it.

https://github.com/nodejs/NG/issues/8#issuecomment-226921556

I'm using the `http2` NPM package for now.

https://github.com/molnarg/node-http2

### CDN
Few CDNs support HTTP/2 connections with origin servers. They mostly use HTTP(S)/1.x only.

https://istlsfastyet.com

- Google Cloud CDN :| Maybe? Haven't managed to test this yet. https://cloudplatform.googleblog.com/2015/10/Full-Speed-Ahead-with-HTTP2-on-Google-Cloud-Platform.html
- Fastly :| Only very recently HTTP/2 in Limited Availability (private). Not sure if it supports origin Push relay https://www.fastly.com/blog/announcing-limited-availability-http2
- AWS CloudFront :( Does not support HTTP/2 at all.
- CloudFlare :( "Full HTTPS" seems to use HTTPS/1.1. Supports hacky `Link` header instead. Test: https://cloudflare-pushdemo.cf.sg
- KeyCDN :( Seems to use HTTPS/1.1 with origin server. Test: https://pushdemo-4792.kxcdn.com/
- MaxCDN :( Not supported as per announcement https://www.maxcdn.com/blog/http2-support/

## What's Next?

### "Sufficiently Advanced" Webserver
The current implementation is naive. Always push-promising all assets could still be wasteful, due to signaling overhead and delayed cancellation by the client. Perhaps use expiration request headers to only push changed assets?

### Tracing Dependencies
Babel transpilation with ES6 modules does not support NPM packages, only local files.

### QUIC
This is a continuation of SPDY R&D. Built on UDP, reinvents TCP. Allows 0-RTT. Not sure how QUIC would support Server Push.

Spec does not mention push promise at all.

https://datatracker.ietf.org/doc/draft-tsvwg-quic-protocol/?include_text=1

Reference implementation does include `PUSH_PROMISE`.

https://github.com/google/proto-quic/search?utf8=%E2%9C%93&q=push_promise&type=Code

### Tyranny of the Rocket Equation
Huge effort require to make incremental performance gains.

http://www.nasa.gov/mission_pages/station/expeditions/expedition30/tryanny.html

## The End

Questions?
