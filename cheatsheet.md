## Cheat Sheet

### Tools

- https://blog.cloudflare.com/tools-for-debugging-testing-and-using-http-2/

### Remote Demo

#### Terminal

```
ssh roo@pushdemo.cf.sg
```

#### Upstart
https://wiki.ubuntu.com/SystemdForUpstartUsers

```
start http-server
stop http-server

start http2server
stop http2server

vim /etc/init/http-server.conf
vim /etc/init/http2server.conf

tail -f /var/log/upstart/http-server.log
tail -f /var/log/upstart/http2server.log
```

#### Letsencrypt

```
/etc/letsencrypt/live/pushdemo.cf.sg/cert.pem
/etc/letsencrypt/live/pushdemo.cf.sg/chain.pem
/etc/letsencrypt/live/pushdemo.cf.sg/fullchain.pem
/etc/letsencrypt/live/pushdemo.cf.sg/privkey.pem
```
