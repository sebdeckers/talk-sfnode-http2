# The Promise of HTTP/2 Push

> HTTP/2 will make our applications faster, simpler, and more robust—a rare combination—by allowing us to undo many of the HTTP/1.1 workarounds previously done within our applications and address these concerns within the transport layer itself.

Ilya Grigorik **High Performance Browser Networking** *Chapter 12: HTTP/2* https://hpbn.co/http2/
